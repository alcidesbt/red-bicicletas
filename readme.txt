El presente proyecto corresponde  corresponde a l módulo I de programación en el lado del servidor con Node.js, Express y MongoDB dicatado por la plataforma de curos "COURSERA".

Agradezco la evaluación del mismo descargándolo en local y corriendo la aplicación web y API creada.

El proyecto incluye lo siguinte:

1. Aplicación web con la visualización de una página de inicio (index.html) descargada de internet y personalizada según las indicaciones del curso.

2. Inclusión de las funcionalidad CRUD (Create, Read, Update y Delete) de objetos bicicleta creadas en memoria y visualizadas o localizadas en unas coordenadas de la ciudad de Puerto La Cruz, Venezuela.

3. Archivos de soporte del CRUD segun el modelo MVC (Model, View, Controller) en las carpetas de la estructura de archivos generada por express Generator.

4. Creación de una API con sus funcionalidades CRUD, las cuales fueron probadas con POSTMAN.

Mi dirección de correo electrónico es: alcides0514@gmail.com

La dirección del repositorio es: https://bitbucket.org/alcidesbt/red-bicicletas/src/master/